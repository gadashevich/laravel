$(document).ready(function(){

  //Плавный скролл

  $("a.scrollto").click(function () {
      var elementClick = '#'+$(this).attr("href").split("#")[1]
      var destination = $(elementClick).offset().top;
      jQuery("html:not(:animated),body:not(:animated)").animate({scrollTop: destination}, 1000);
      return false;
  });
 
  $("html, body, .mfp-zoom-out-cur").niceScroll({
    horizrailenabled : false,
    "verge" : "500"
  });

  $('.menu .menu_item a').click(function(){
      $('header .menu .menu_item').removeClass('active');
      $(this).parent().addClass('active');
  });

  //Гамбургер меню

  var forEach=function(t,o,r){if("[object Object]"===Object.prototype.toString.call(t))for(var c in t)Object.prototype.hasOwnProperty.call(t,c)&&o.call(r,t[c],c,t);else for(var e=0,l=t.length;l>e;e++)o.call(r,t[e],e,t)};
    var hamburgers = document.querySelectorAll(".hamburger");
    if (hamburgers.length > 0) {
      forEach(hamburgers, function(hamburger) {
        hamburger.addEventListener("click", function() {
          this.classList.toggle("is-active");
        }, false);
      });
    }

  $(".hamburger").click(function(){
    $("nav").slideToggle(300);
  });

  $('nav .hamburger__navigation__items a.navs__href, nav a.btn__blue.scrollto').click(function(){
    $("nav").slideToggle(300);
    $(".hamburger").toggleClass("is-active");
  });

  //Анимация гамбургер меню
  $(function() {
    $("header nav #hamburger__sidebar .hamburger__navigation__items, #first div.btn_transperent").animated("fadeInLeft");
    $("#hamburger__sidebar .btn__blue").animated("fadeInUp")
  });

  $(window).scroll(function(){
    if($(this).scrollTop() > $(this).height()){
      $('.top').addClass('top__active');
    }else{
      $('.top').removeClass('top__active');
    }
  });

  $('.top').click(function(){
    $('html, body').stop().animate({scrollTop:0},"slow","swing");
  });

});