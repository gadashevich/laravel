<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Rss extends Model
{
    protected $fillable = ['name','link'];
    //
}
