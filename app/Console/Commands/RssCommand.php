<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;
use App\RssPost;



class RssCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'rss:parse';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $rsses = DB::table('rsses')->get();
        if(!empty($rsses)){
            foreach ($rsses as $rss){
                if(!empty($rss->link))
                    $this->parse($rss);
            }
        }
    }

    /**
     * Parse rss feed.
     *
     * @return bool
     */
    protected function parse($rss){

        $xml = simplexml_load_file($rss->link, null, LIBXML_NOCDATA);
        $entries = $xml->xpath("//item");

        foreach ($entries as $entry){
            $post = RssPost::where('guid', (string)$entry->guid[0])->first();
            if(!$post){
                $post = new RssPost();
                $post->rss_id = (int)$rss->id;
                $post->title = (string)$entry->title[0];
                $post->link = (string)$entry->link[0];
                $post->description = (string)$entry->description[0] ;
                $post->guid = (string)$entry->guid[0];
                $post->image = "";
                $post->save();
            }else{
                $post->title = (string)$entry->title[0];
                $post->description = (string)$entry->description[0] ;
                $post->image = "";
                $post->save();
            }

        }
        return true;
    }
}
