<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\RssPost;

class ApiController extends Controller
{
    //
    public function rss()
    {
        $response = RssPost::paginate(15);
        return response()->json($response, 200);
    }
}
