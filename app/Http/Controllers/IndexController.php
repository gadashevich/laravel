<?php

namespace App\Http\Controllers;

use App\Mail\SendLink;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
//use Illuminate\Contracts\Mail\Mailer;

use App\Jobs\SendReminderEmail;

class IndexController extends Controller
{
    public function index()
    {
        return view('welcome');
    }

    public function send(Request $request)
    {

        $this->dispatch(new SendReminderEmail('test@test.te'));
      

    }

    public function success()
    {
        return view('success');
    }
}
