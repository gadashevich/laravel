<?php

return [


    "driver" => "smtp",
    "host" => "smtp.mailtrap.io",
    "port" => 2525,
    "from" => array(
        "address" => "from@example.com",
        "name" => "Example"
    ),
    "username" => "b8d836b30e58fc",
    "password" => "4f83f03cd818ad",
    "sendmail" => "/usr/sbin/sendmail -bs",
    "pretend" => false,


    'markdown' => [
        'theme' => 'default',

        'paths' => [
            resource_path('views/vendor/mail'),
        ],
    ],
];
