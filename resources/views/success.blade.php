
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>Янтарный Айсберг производитель воды высшей категории качества</title>
    <meta name="keywords" content="Янтарный Айсберг, айсберг, вода, качество, Калининград, кулер для воды, доставка, на дом,  в офис, Зеленоградские источники, вода питьевая, питьевая, бутилированная, бутылированная, ассортимент, качество, заказ, water" />
    <meta name="description" content="Доставка питьевой воды в Калининграде и области. Вода высшей категории качества" />
    <link rel="stylesheet" type="text/css" href="http://api.iceberg-aqua.ru/css/bootstrap.css" />
    <link rel="stylesheet" type="text/css" href="http://api.iceberg-aqua.ru/css/datepicker3.css" />
    <link rel="stylesheet" type="text/css" href="http://api.iceberg-aqua.ru/css/jquery.kladr.min.css" />
    <link rel="stylesheet" type="text/css" href="http://api.iceberg-aqua.ru/css/lightSlider.css" />
    <link rel="stylesheet" type="text/css" href="/css/style.css" />


    <!-- modal window 1, window 2, window 3 -->
    <!-- Latest compiled and minified CSS https://silviomoreto.github.io/bootstrap-select/ -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.12.4/css/bootstrap-select.min.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">

    <!-- modal window 1, window 2, window 3 -->
    <!-- stylesheet -->
    <link rel="stylesheet" href="http://api.iceberg-aqua.ru/css/iceberg.css">
    <!-- modal window 1, window 2, window 3 -->
    <link rel="icon" href="media/images/favicon.ico" type="image/vnd.microsoft.icon" /><link rel="shortcut icon" href="media/images/favicon.ico" type="image/vnd.microsoft.icon" />
</head>

<body>
<div id="wrapper">
    <div id="success" class="modal-dialog modal-lg modal-wrapper">
        <div class="modal-content">

            <div class="modal-body">
                <div class="success-content">
                    <img class="success-content__check" src="http://api.iceberg-aqua.ru/img/check.png" alt="success">
                    <h4 class="success-content__info">Ваше письмо<br>успешно отправлено</h4>
                    <h6 class="success-content_delivery">Если не можете найти письмо от нас, пожалуйста, проверьте папку спам.</h6>
                    <a href="http://iceberg-aqua.ru" title="" id="get-back" class="btn btn_submit success-content__back">Перейти на главную страницу</a>
                </div>
            </div>
        </div>
    </div>
</div>
</body>
</html>
