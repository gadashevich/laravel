<!DOCTYPE html>
<html lang="ru">
<head>

    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content=" Быстрое и удобное приложение от компании «Янтарный АЙСБЕРГ» позволит вам сделать заказ воды, чая, кофе, одноразовой посуды и других сопутствующих товаров. Служба доставки позволяет оперативно доставлять питьевую воду в офисы, дома и квартиры жителей всей Калининградской области.Важно знать – приложение бесплатное, как и доставка от компании. Выбирайте количество баллонов, указывайте адрес - готово.Гарантируем доставку в выбранное вами время. Быстрый повтор заказа. Получайте уведомления об акциях. Оплачивайте банковской картой или наличными.">
    <meta name="keywords" content="доставка воды в калининграде, советске. Янтарный Айсберг, айсберг, вода, качество, Калининград, кулер для воды, доставка, на дом,  в офис, Зеленоградские источники, вода питьевая, питьевая, бутилированная, бутылированная, ассортимент, качество, заказ, water, мобильное приложение, доставка воды через мобильное приложение, быстрая доставка">

    <!-- Фавикон -->
    <link rel="shortcut icon" sizes="64X63>" href="/img/favicon.png" type="image/png">

    <!-- meta социальные сети -->
    <meta name="twitter:title" content="Айсберг">
    <meta name="twitter:description" content="Быстрый и удобный заказ воды">
    <meta name="twitter:image:src" content="http://acpool.ru/img/socials.jpg">
    <meta name="twitter:site" content="Айсберг">
    <meta property="og:type" content="website">
    <meta property="og:locale" content="ru_RU">
    <meta property="og:url" content="http://acpool.ru//">
    <meta property="og:site_name" content="Айсберг">
    <meta property="og:description" content="Быстрый и удобный заказ воды">
    <meta property="og:title" content="Айсберг">
    <meta property="og:image" content="http://www.acpool.ru/img/socials.jpg">
    <meta property="og:image:width" content="604">
    <meta property="og:image:height" content="322">

    <title>Айсберг</title>

    <!-- Поддержка старыми IE6,7,8 элементров от HTML5 -->
    <script src="/js/html5shiv.min.js"></script>

    <link rel="stylesheet" href="/css/mail_vendor.css">

    <!-- В файл vendor.css включены следующие файлы стилей плагинов-->
    <!-- Bootstrap-->
    <!-- Шрифтовые иконки Font-awesome-->
    <!-- Анимация на сайте-->

    <!-- Основной файл стилей -->
    <link rel="stylesheet" href="/css/mail.css">

    <!-- Chrome, Firefox OS and Opera -->
    <meta name="theme-color" content="#DBF3FC">
    <!-- Windows Phone -->
    <meta name="msapplication-navbutton-color" content="#DBF3FC">
    <!-- iOS Safari -->
    <meta name="apple-mobile-web-app-status-bar-style" content="#DBF3FC">

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

    <!-- <script src="https://www.google.com/recaptcha/api.js?onload=reCaptchaOnLoadCallback&render=explicit"></script> -->

</head>

<body>

<!-- //= template/modernizr.html -->

<!-- <div class="preloader">
	<div class="loader-section section-left text-center">
	</div>
	<div class="loader-section section-right text-center">
	</div>
	<div class="pulse anti"></div>
</div> -->



<section id="first">

    <header>
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="header_container">
                        <a href="http://iceberg-aqua.ru">
                            <div class="logo">
                                <img src="/img/logo.png" alt="logo" class="img-responsive">
                            </div>
                        </a>
                        <a href="http://iceberg-aqua.ru" class="btn__transparent hvr-sweep-to-top">Вернуться на главную</a>
                    </div>
                </div>
            </div>
        </div>
    </header>

    <div class="container">
        <div class="row">
            <div class="col-md-6">
                <div class="mobiles">
                    <img src="/img/mobiles_1.png" alt="Мобильные телефоны" class="img-responsive">
                </div>
            </div>
            <div class="col-md-6">
                <div class="caption">
                    <h1>Быстрый<br class="br_mobile"> и удобный<br> заказ воды</h1>
                    <p class="email_text">Оставьте адрес лектронной почты<br> мы вышлем ссылку на приложение</p>
                    <form class="order js-form" method="post" action="#" id="popupResult">
                        <input type="text" name="mail" class="order__input" placeholder="Введите Ваш E-mail:" required id="email"><br>
                        <button class="btn__green">Получить ссылку</button>
                    </form>
                    @if ($errors->any())
                        <div class="danger">
                            @foreach ($errors->all() as $error)
                                {{ $error }}
                            @endforeach
                        </div>
                    @endif
                    <p class="start">Начните заказывать воду через приложение прямо сейчас</p>
                    <div class="market">
                        <a href="https://itunes.apple.com/ru/app/айсберг/id1288250696?mt=8&ign-mpt=uo%3D4"><img src="/img/app.png" alt="AppStore" class="img-responsive"></a>
                        <a href="https://play.google.com/store/apps/details?id=rewarded.mobiap.com.icebegr"><img src="/img/google.png" alt="GooglePlay" class="img-responsive"></a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<section id="shipping_order">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h2>Заказ доставки питьевой воды<br>в три простых шага</h2>
            </div>
        </div>
        <div class="row">
            <div class="col-md-4">
                <div class="step text-center">
                    <img src="/img/step1.png" alt="Alt" class="img-responsive">
                    <p class="step_caption text-center">Закажи воду</p>
                    <p class="step_description text-center">Легкой горизонтальной прокруткой выберите любое количество баллонов для доставки. Вверху справа и прямо надо баллонами отобразится цена.</p>
                </div>
            </div>
            <div class="col-md-4">
                <div class="step">
                    <img src="/img/step2.png" alt="Alt" class="img-responsive">
                    <p class="step_caption text-center">Выбери дополнительную продукцию</p>
                    <p class="step_description text-center">Выберите необходимый товар и нажмите на зеленую кнопку с ценой. Товар добавится в корзину. Интуитивный и удобный каталог<br> товаров с вертикальной прокруткой<br> по категориям и горизонтальной<br> прокруткой по товарам<br> в каждой категории. </p>
                </div>
            </div>
            <div class="col-md-4">
                <div class="step">
                    <img src="/img/step3.png" alt="Alt" class="img-responsive">
                    <p class="step_caption text-center">Оформи заказ</p>
                    <p class="step_description text-center">Выберите адрес доставки и способ оплаты «Банковской картой» или «Наличными курьеру». Сделав заказ, вы убедитесь, что мобильное приложение от компании Янтарный Айсберг» - действительно быстрый и удобный способ доставки питьевой воды.</p>
                </div>
            </div>
        </div>
    </div>
</section>

<section id="aditional_features">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h2>Дополнительные возможности</h2>
            </div>
        </div>
        <div class="row">
            <div class="col-md-10 col-md-offset-1">
                <div class="row">
                    <div class="description_wrapper right_img">
                        <div class="col-md-6 order_block">
                            <div class="features_description">
                                <h3>Повторный заказ</h3>
                                <p>На главном экране мобильного приложения Айсберг - доставка питьевой воды, нажмите на кнопку «Повторить заказ». Вы минуете экраны с выбором количества баллонов и дополнительной продукции в каталоге товаров и сразу попадете на страницу «Ваш заказ». В корзине будут лежать все ваши товары из последнего успешного заказа. Теперь просто нажмите кнопку «Оформить заказ»</p>
                            </div>
                        </div>
                        <div class="col-md-6 order_block">
                            <div class="description_img">
                                <img src="/img/features1.png" alt="Alt" class="img-responsive">
                            </div>
                        </div>
                    </div>
                    <div class="description_wrapper left_img">
                        <div class="col-md-6 order_block">
                            <div class="description_img">
                                <img src="/img/features2.png" alt="Alt" class="img-responsive">
                            </div>
                        </div>
                        <div class="col-md-6 order_block">
                            <div class="features_description">
                                <h3>Изменения остатков воды в баллонах</h3>
                                <p>Мы вовремя напомним вам о том, что вода заканчивается. Мы рассчитываем ваше потребление воды каждый день и отображаем расчеты в виде изменений уровня воды в баллонах на главном экране. Для более точных данных мы добавили интересную функцию, которая позволяет Вам самостоятельно регулировать количество воды в баллонах.</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="description_wrapper left_img">
                <div class="col-md-7 order_block">
                    <img src="/img/features4.png" alt="Alt" class="img-responsive">
                </div>
                <div class="col-md-5 order_block">
                    <div class="features_description">
                        <h3>Выбор способа оплаты</h3>
                        <p>Мы добавили возможность оплаты с помощью банковской карты. Выбирайте оплату по карте и нажмите «Оформить заказ». Достаточно ввести свои данные карты и e-mail в окне оплаты Сбербанка, чтобы при следующем заказе не вводить данные повторно.</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<section id="natural_origin">
    <div class="container">
        <div class="row">
            <div class="natural_origin-wrapper">
                <div class="col-md-7">
                    <div class="origin_caption">
                        <h2>Природное происхождение.</h2>
                        <p>Питьевая вода «Янтарный АЙСБЕРГ» добывается в экологически чистой лесной зоне вдали от промышленных предприятий и автомобильных трасс.
                            На территории завода расположены артезианские скважины глубиной до 132 метров.Здоровая энергетика и сбалансированный состав делают воду высшей категории полезной, и её можно использовать как для питья, так и для приготовления блюд.</p>
                    </div>
                </div>
                <div class="col-md-5">
                    <div class="origin_img">
                        <img src="/img/origin.png" alt="Alt" class="img-responsive">
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<section id="free_application">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h2>Установите бесплатное<br>приложение на ваш смартфон!</h2>
                <div class="apps_link">
                    <a href="https://itunes.apple.com/ru/app/айсберг/id1288250696?mt=8&ign-mpt=uo%3D4">
                        <img src="/img/app_f.png" alt="Alt" class="img-responsive">
                    </a>
                    <a href="https://play.google.com/store/apps/details?id=rewarded.mobiap.com.icebegr">
                        <img src="/img/google_f.png" alt="Alt" class="img-responsive">
                    </a>
                </div>
                <a href="http://iceberg-aqua.ru/protess-oplaty/" target="_blank" style="color:#fff;"><p class="text-center">Политика конфиденциальности</p></a>
            </div>
        </div>
    </div>
</section>

<!-- Кнопка "Вверх" -->
<div class="top hvr-sink" title="Наверх"><i class="fa fa-angle-double-up"></i></div>

<script src="/js/mail_vendor.js"></script>
<!-- В файл vendor.js включены следующие скрипты -->
<!-- JQuery-->
<!-- <script src="js/jquery.min.js"></script>
<script src="js/jquery-migrate-3.0.0.min.js"></script> -->

<!-- Bootstrap-->
<!-- <script src="js/bootstrap.min.js"></script>-->

<!-- Валидация -->
<script type="text/javascript" src="https://ajax.aspnetcdn.com/ajax/jquery.validate/1.14.0/jquery.validate.js"></script>

<!--Кастомные скрипты -->
<script src="/js/mail.js"></script>

<!--Вёрстка /Каминский Дмитрий mail:webmaster@kaminskiydmitriy.com, skype: drakonfire -->

</body>
</html>