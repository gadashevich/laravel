@extends('admin.layouts.app_admin')

@section('content')
    <div class="container">
        @component('admin.components.breadcrumbs')
            @slot('title') Создать  ленту @endslot
        @endcomponent
        <hr/>
        <form class="form-horizontal" method="post" action="{{route('admin.rss.store')}}">
            {{csrf_field()}}
            @include('admin.rss.partians.form')
        </form>

@endsection