@extends('admin.layouts.app_admin')

@section('content')
    <div class="container">
        @component('admin.components.breadcrumbs')
            @slot('title') Редактировать  ленту @endslot
        @endcomponent
        <hr/>
        <form class="form-horizontal" method="post" action="{{route('admin.rss.update',$rss)}}">
            {{csrf_field()}}
            <input type="hidden" name="_method" value="put">
            @include('admin.rss.partians.form')
        </form>

@endsection