@extends('admin.layouts.app_admin')

@section('content')
    <div class="container">
        @component('admin.components.breadcrumbs')
            @slot('title') Список rss @endslot
        @endcomponent
        <hr/>

            <a href="{{route('admin.rss.create')}}">Создать ленту</a>
            <table>
                <thead>
                    <th>Название</th>
                    <th>Ссылка</th>
                    <th>Действие</th>
                </thead>
                <tbody>
                @forelse($rss as $key=>$value)
                    <tr>
                        <td>{{$value->name}}</td>
                        <td>{{$value->link}}</td>
                        <td>
                            <a href="{{route('admin.rss.edit',$value)}}">Редактировать</a>
                            <form action="{{route('admin.rss.destroy',$value)}}" onsubmit="if(confirm('Вы уверены?')){return true;}else{return false}" method="post">
                                <input type="hidden" name="_method" value="DELETE">
                                {{csrf_field()}}
                                <button type="submit" class="btn">Удалить</button>
                            </form>
                        </td>
                    </tr>
                @empty
                    <tr><td colspan="3">Пусто</td></tr>
                @endforelse
                </tbody>
                <tfoot>
                <tr>
                    <td colspan="3">
                        <ul>
                            {{$rss->links()}}
                        </ul>
                    </td>
                </tr>
                </tfoot>
            </table>
    </div>
@endsection